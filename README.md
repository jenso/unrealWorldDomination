# Unreal World Domination
In diesem Repository befinden sich die Projektdateien zu den zwei
Unrealprojekten,  die innerhalb des World Domination Seminars im WS17/18 der HfG
Karlsruhe entstanden sind.

## Hardware
Verwendet wurde eine HTC Vive sowie ein Steamcontroller.

## Starter Content
Der in der Unreal Engine verwendete starter content wurde absichtlich nicht
diesem Repository beigefügt.

## Runner - Das Geschwindigkeitsspiel
### Anleitung
Zu Beginn des Spiels wird die maximale Geschwindigkeit des Spielers festgelegt.
Anschließend muss dieser versuchen, für fünf Sekunden innerhalb des roten
Rechtecks zu stehen. Gelingt dies, so wird das Rechteck neu plaziert und der
Spieler kann sich erneut dran versuchen.

### Screenshots
#### Hauptmenü
![Main Menu](./Runner/screenshots/menu.png "MainMenu")

#### Im Spiel
![ingame](./Runner/screenshots/ingame.png "ingame")

## Labyrinth - Das Sichtspiel
Der Spieler wählt zu Beginn des Spieles eine Sichtmodifikation aus. Diese legt
anschließend Fest, wie die Sicht des Spielers beim Entkommen aus dem Labyrinth
ist. Zur Auswahl stehen Maus, Giraffe, Fledermaus, Schaf(2). Während bei der
Maus bzw. der Giraffe jeweils die Größe angepasst wurde, ist die Sicht der
Fledermaus an ein Echolot angelehnt. Die Schafsicht hingegen lässt den Spieler
die Welt in einer 160° Projektion bzw. tatsächlich unabhängigen Bilder für jedes
Auge, wahrnehmen.


### Screenshots
#### Hauptmenü
![Main Menu](./screenshots/menu.png "MainMenu")

#### Maus
![Maus](./screenshots/maus.png "maus")

#### Giraffe
![Giraffe](./screenshots/giraffe.png "giraffe")

#### Fledermaus
![Fledermaus](./screenshots/fledermaus1.png "fledermaus")
![Fledermaus](./screenshots/fledermaus2.png "fledermaus")


#### Schaf 1: 160°
![Schaf](./screenshots/schaf.png "schaf")

#### Schaf2: Einzelne Bilder für jedes Auge
![Schaf2](./screenshots/schaf2.png "schaf2")